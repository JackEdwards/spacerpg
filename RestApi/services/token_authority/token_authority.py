#!/usr/bin/env python3.6
import datetime
from functools import wraps
import json
import os
import secrets

from .token_repo import ApplicationTokenRepo, DeviceTokenRepo, RefreshTokenRepo
from .models import ApplicationToken, DeviceToken, RefreshToken
from .modules import Database, Logger

class TokenAuthority:
    
    def __init__(self):
        self.application_key = self.load_key('application')
        self.device_key = self.load_key('device')
        self.refresh_key = self.load_key('refresh')

        now = datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')
        if self.refresh_key['expires'] < now:
            self.refresh_key = self.generate_new_key('refresh')

    def load_key(self, name):
        this_directory = os.path.dirname(os.path.abspath(__file__))
        key_file = os.path.join(this_directory, '{}_key.json'.format(name))
        if not os.path.exists(key_file):
            return self.generate_new_key(name)
        else:
            with open(key_file, 'r') as key_data:
                return json.load(key_data)

    def generate_new_key(self, name):
        this_directory = os.path.dirname(os.path.abspath(__file__))
        key_file = os.path.join(this_directory, '{}_key.json'.format(name))

        if name == 'refresh':
            expires = (datetime.datetime.utcnow() + datetime.timedelta(days=28)).strftime('%Y-%m-%d %H:%M:%S')
        else:
            expires = None

        key = {
            'created': datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
            'expires': expires,
            'value': secrets.token_urlsafe(256)
        }

        if os.path.exists(key_file):
            raise ValueError('Cannot overwrite application or device keys')

        with open(key_file, 'w') as key_file:
            json.dump(key, key_file)
        return key
    
    #  Use only as a decorator
    def require_verified_application_token(func):
        @wraps(func)
        def inner(*args, **kwargs):
            print(*args)
            print(**kwargs)
            exit()
            app_token = ApplicationToken()
            app_token.jwt = token_jwt

            #  Verify the token's signature
            decoded = app_token.decode_jwt(key=self.application_key['value'], algorithm='HS256')
            if decoded['verified']:
                #  Verify the token exists in the database
                db_app_token = ApplicationTokenRepo().get_by_id(decoded['payload']['jti'])
                if (db_app_token.jwt == app_token.jwt) and (db_app_token.status == 1):
                    #  Everything is good
                    return func(*args, **kwargs)

            return {
                'success': False,
                'error': 'invalid_application_token'
            }
        return inner

    #  Generate a new application token; return the token's jwt
    #  A token object and it's jwt get saved to a database
    #  Application tokens do not require the verification of other token types
    def new_application_token(self, application):
        #  Make a new token object and handle token logic
        app_token = ApplicationToken(application=application, status=0)
        app_token.payload = app_token.new_payload()
        app_token.jwt = app_token.new_jwt(key=self.application_key['value'], algorithm='HS256')

        #  Save the token to the database
        ApplicationTokenRepo().insert(app_token)

        #  Return the jwt to the application; that's all the application is asking for
        return {
            'success': True,
            'jwt': app_token.jwt
        }

    #  Generate a new device token; return the token's jwt
    #  A token object and it's jwt get saved to a database
    #  Device tokens require a verified application token to be passed in
    @require_verified_application_token
    def new_device_token(self, device, application_jwt):
        #  Verify the application_jwt:
        #  1.  Check the signature
        app_token = ApplicationToken()
        app_token.jwt = application_jwt
        decoded = app_token.decode_jwt(key=self.application_key['value'], algorithm='HS256')
        if decoded['verified']:
            #  2.  Verify the token is in the database
            db_app_token = ApplicationTokenRepo().get_by_id(decoded['payload']['jti'])
            if (db_app_token.jwt == app_token.jwt) and (db_app_token.status == 1):
                #  Make a new device token and handle the token logic
                device_token = DeviceToken(device=device, status=1)
                device_token.payload = device_token.new_payload()
                device_token.jwt = device_token.new_jwt(key=self.device_key['value'], algorithm='HS256')

                #  Save the token to the database
                DeviceTokenRepo().insert(device_token)

                #  Return the jwt to the application; that's all the application is asking for
                return {
                    'success': True,
                    'jwt': device_token.jwt
                }
            else:
                return {
                    'success': False,
                    'error': 'invalid_application_token'
                }
        else:
            return {
                'success': False,
                'error': 'invalid_application_token'
            }

    #  Generate a new refresh token; return the token's jwt
    #  A token object and it's jwt get saved to a database
    #  Refresh tokens require verified device and application tokens to be passed in
    def new_refresh_token(self, user_id, device_jwt, application_jwt):
        #  Verify the application_jwt:
        #  1.  Check the signature
        app_token = ApplicationToken()
        app_token.jwt = application_jwt
        decoded_app = app_token.decode_jwt(key=self.application_key['value'], algorithm='HS256')
        if decoded_app['verified']:
            #  2.  Verify the token is in the database
            db_app_token = ApplicationTokenRepo().get_by_id(decoded_app['payload']['jti'])
            if (db_app_token.jwt == app_token.jwt) and (db_app_token.status == 1):
                #  Verify the device_jwt:
                #  1.  Check the signature
                device_token = DeviceToken()
                device_token.jwt = device_jwt
                decoded_device = device_token.decode_jwt(key=self.device_key['value'], algorithm='HS256')
                if decoded_device['verified']:
                    pass
                else:
                    return {
                        'success': False,
                        'error': 'invalid_device_token'
                    }
            else:
                return {
                    'success': False,
                    'error': 'invalid_application_token'
                }
        else:
            return {
                'success': False,
                'error': 'invalid_application_token'
            }
