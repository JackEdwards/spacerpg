#!/usr/bin/env python3
import datetime
import inspect
import os
import sys


class Logger:
    def __init__(self):
        self.config = {
            'LOG_FILE_DIRECTORY': sys.path[0],
            'LOG_FILE_NAME': 'log',
            'LOG_FILE_EXTENSION': '.txt',
            'EXCEPTION_FILE_DIRECTORY': sys.path[0],
            'EXCEPTION_FILE_NAME': 'log',
            'EXCEPTION_FILE_EXTENSION': '.txt.exc',
            'WRITE_DEBUG_LOGS': False,
            'WRITE_TRACE_LOGS': False
        }

    @property
    def log_file_path(self):
        return '{}{}{}{}'.format(
            self.config['LOG_FILE_DIRECTORY'],
            os.sep,
            self.config['LOG_FILE_NAME'],
            self.config['LOG_FILE_EXTENSION']
        )

    @property
    def exc_file_path(self):
        return '{}{}{}{}'.format(
            self.config['EXCEPTION_FILE_DIRECTORY'],
            os.sep,
            self.config['EXCEPTION_FILE_NAME'],
            self.config['EXCEPTION_FILE_EXTENSION']
        )

    def log(self, level=5, message=None, exception=None):
        if level == 4 and not self.config['WRITE_DEBUG_LOGS']:
            return True

        if level == 5 and not self.config['WRITE_TRACE_LOGS']:
            return True

        now = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        level_string = {
            0: 'SECURITY',
            1: 'FATAL',
            2: 'ERROR',
            3: 'INFO',
            4: 'DEBUG',
            5: 'TRACE'
        }
        if level not in level_string:
            level_string[level] = 'INVALID'

        if message:
            try:
                stack = inspect.stack()[1]
                with open(self.log_file_path, 'a') as f:
                    s = '{} | {} | {} | {} | {}\n'.format(now, level_string[level], stack.filename, stack.function, message)
                    f.write(s)
            except Exception:  # Don't have a reason to handle different exceptions differently right now
                print('Unhandled exception occurred: {}'.format(sys.exc_info()))

        if exception:
            try:
                stack = inspect.stack()[1]
                with open(self.exc_file_path, 'a') as f:
                    s = '{} | {} | {} | {} | {}\n'.format(now, level_string[level], stack.filename, stack.function, exception)
                    f.write(s)
            except Exception:  # Don't have a reason to handle different exceptions differently right now
                print('Unhandled exception occurred: {}'.format(sys.exc_info()))

        return True
