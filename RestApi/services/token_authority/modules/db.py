from .logger import Logger
import json
import os
import sqlite3
import sys

class Database:

    def __init__(self):
        this_dir = os.path.dirname(os.path.abspath(__file__))
        parent_dir = os.path.dirname(this_dir)
        config_file = os.path.join(parent_dir , 'config.json')
        with open(config_file) as json_data:
            config = json.load(json_data)
        
        self.logger = Logger()
        self.logger.config['LOG_FILE_DIRECTORY'] = config['log_config']['log_path'] or parent_dir
        self.logger.config['WRITE_DEBUG_LOGS'] = config['log_config']['debug']
        self.logger.config['WRITE_TRACE_LOGS'] = config['log_config']['trace']
        self.connection = sqlite3.connect(config['database_config']['sqlite3_config']['file_path'] or os.path.join(parent_dir, 'main.db'))
        self.connection.row_factory = sqlite3.Row # This let's us do cursor.fetchone().keys()
        self.cursor = self.connection.cursor()

    #  "SELECT"
    def query(self, query, args={}):
        self.logger.log(level=5, message='query: [{}] | args: [{}]'.format(query, args))
        try:
            self.cursor.execute(query, args)
            return self.cursor.fetchall()
        except sqlite3.Error as e:
            self.logger.log(level=2, message='Exception occurred querying the database', exception=e)

    #  "CREATE", "INSERT", "UPDATE"
    def update(self, query, args={}):
        self.logger.log(level=5, message='query: [{}] | args: [{}]'.format(query, args))
        try:
            self.cursor.execute(query, args)
            self.connection.commit()
            return True
        except sqlite3.Error as e:
            self.logger.log(level=2, message='Exception occurred updating the database', exception=e)
            self.connection.rollback()
            return False

    def __del__(self):
        self.connection.close()