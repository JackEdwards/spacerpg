from .models import ApplicationToken, DeviceToken, RefreshToken
from .modules import Database

class ApplicationTokenRepo:

    def __init__(self):
        self.db = Database()
        self.create_table_if_not_exists()

    #  Convert a single record to a TokenObject
    def deserialize(self, named_record):
        token_object = ApplicationToken(
            id=named_record['id'],
            application=named_record['application'],
            status=named_record['status'],
            created=named_record['created']
        )
        token_object.jwt = named_record['jwt']
        return token_object

    #  Convert a TokenObject into a dict
    def serialize(self, token_object):
        return {
            'id': token_object.id,
            'application': token_object.aud,
            'jwt': token_object.jwt,
            'status': token_object.status,
            'created': token_object.created
        }

    #  Create a "token" record for the passed in TokenObject
    def insert(self, token_object):
        query = ('INSERT INTO token_application ('
            'id,'
            'application,'
            'jwt,'
            'status,'
            'created'
            ') VALUES ('
            ':id,'
            ':application,'
            ':jwt,'
            ':status,'
            ':created);')
        args = self.serialize(token_object)
        self.db.update(query, args)

    #  Get a "token" record by it's "id"
    def get_by_id(self, key):
        query = ('SELECT * FROM token_application WHERE id=:id;')
        args = {'id': key}
        named_record = self.db.query(query, args)

        #  De-serialize the result
        token_object = self.deserialize(named_record)
        return token_object

    #  Delete a "token" record by it's "id"
    def delete_by_id(self, key):
        query = ('DELETE FROM token_application WHERE id=:id;')
        args = {'id': key}
        self.db.update(query, args)

    #  Create the "token" table if it does not exist
    def create_table_if_not_exists(self):
        query = ('CREATE TABLE IF NOT EXISTS token_application ('
            'id TEXT PRIMARY KEY,'
            'application,'
            'status,'
            'jwt,'
            'created);')
        self.db.update(query)

class DeviceTokenRepo:

    def __init__(self):
        self.db = Database()
        self.create_table_if_not_exists()

    #  Convert a single record to a TokenObject
    def deserialize(self, named_record):
        keys = named_record.keys()
        token_object = DeviceToken()
        for key in keys:
            setattr(token_object, key, named_record[key])
        return token_object

    #  Convert a TokenObject into a dict
    def serialize(self, token_object):
        token_dict = {}
        for key, value in token_object.__dict__.items():
            token_dict[key] = value
        return token_dict

    #  Create a "token" record for the passed in TokenObject
    def insert(self, token_object):
        query = ('INSERT INTO token_device ('
            'id,'
            'device,'
            'status,'
            'jwt,'
            'created'
            ') VALUES ('
            ':id,'
            ':device,'
            ':status,'
            ':jwt,'
            ':created);')
        args = self.serialize(token_object)
        self.db.update(query, args)

    #  Get a "token" record by it's "id"
    def get_by_id(self, key):
        query = ('SELECT * FROM token_device WHERE id=:id;')
        args = {'id': key}
        named_record = self.db.query(query, args)

        #  De-serialize the result
        token_object = self.deserialize(named_record)
        return token_object

    #  Delete a "token" record by it's "id"
    def delete_by_id(self, key):
        query = ('DELETE FROM token_device WHERE id=:id;')
        args = {'id': key}
        self.db.update(query, args)

    #  Create the "token" table if it does not exist
    def create_table_if_not_exists(self):
        query = ('CREATE TABLE IF NOT EXISTS token_device ('
            'id TEXT PRIMARY KEY,'
            'device,'
            'status,'
            'jwt,'
            'created);')
        self.db.update(query)


class RefreshTokenRepo:

    def __init__(self):
        self.db = Database()
        self.create_table_if_not_exists()

    #  Convert a single record to a TokenObject
    def deserialize(self, named_record):
        keys = named_record.keys()
        token_object = RefreshToken()
        for key in keys:
            setattr(token_object, key, named_record[key])
        return token_object

    #  Convert a TokenObject into a dict
    def serialize(self, token_object):
        token_dict = {}
        for key, value in token_object.__dict__.items():
            token_dict[key] = value
        return token_dict

    #  Create a "token" record for the passed in TokenObject
    def insert(self, token_object):
        query = ('INSERT INTO token_refresh ('
            'id,'
            'user_id,'
            'expiration,'
            'jwt,'
            'created'
            ') VALUES ('
            ':id,'
            ':user_id,'
            ':expiration,'
            ':jwt,'
            ':created);')
        args = self.serialize(token_object)
        self.db.update(query, args)

    #  Get a "token" record by it's "id"
    def get_by_id(self, key):
        query = ('SELECT * FROM token_refresh WHERE id=:id;')
        args = {'id': key}
        named_record = self.db.query(query, args)

        #  De-serialize the result
        token_object = self.deserialize(named_record)
        return token_object

    #  Delete a "token" record by it's "id"
    def delete_by_id(self, key):
        query = ('DELETE FROM token_refresh WHERE id=:id;')
        args = {'id': key}
        self.db.update(query, args)

    #  Create the "token" table if it does not exist
    def create_table_if_not_exists(self):
        query = ('CREATE TABLE IF NOT EXISTS token_refresh ('
            'id TEXT PRIMARY KEY,'
            'user_id,'
            'expiration,'
            'jwt,'
            'created);')
        self.db.update(query)