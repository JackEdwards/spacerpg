#!/usr/bin/env python3.6
import datetime
import uuid
from .json_web_token import Token

class BaseToken:

    def __init__(self,
                token_id,
                token_subject,
                token_audience,
                token_expiration,
                token_created):

        #  Public properties
        self.id = token_id
        self.sub = token_subject
        self.aud = token_audience
        self.exp = token_expiration
        self.iat = int(datetime.datetime.strptime(
            token_created, '%Y-%m-%d %H:%M:%S').timestamp())

        #  Private properties
        self._payload = None
        self._jwt = None # '{}.{}.{}'

    @property
    def payload(self):
        return self._payload

    #  Allow the TokenAuthority to set the payload
    @payload.setter
    def payload(self, value):
        if not self._payload:
            self._payload = value
        else:
            raise ValueError('Cannot overwrite payload')

    #  Generate a new payload for a brand new token
    def new_payload(self):
        return {
                'iss': 'rest.spacerpg.com', # todo, load from config
                'sub': self.sub,
                'aud': self.aud,
                'exp': self.exp,
                'iat': self.iat,
                'jti': self.id
        }

    @property
    def jwt(self):
        return self._jwt

    #  Allow the TokenAuthority to set the jwt
    @jwt.setter
    def jwt(self, value):
        if not self._jwt:
            self._jwt = value 
        else:
            raise ValueError('Cannot overwrite jwt')
                   
    #  Generate a new jwt for a brand new token
    def new_jwt(self, key, algorithm):
        if self.payload:
            return Token().encode(self.payload, key, algorithm)
        else:
            raise ValueError('Cannot create new jwt before payload has been set')

    #  Decode the jwt
    def decode_jwt(self, key, algorithm):
        if self.jwt:
            return Token().decode(self.jwt, key, algorithm)
        else:
            raise ValueError('Cannot decode jwt before jwt has been set')


class ApplicationToken(BaseToken):

    def __init__(self,
                 id=None,
                 application=None,
                 status=None,
                 created=None):

        self.status = status
        self.created = created or datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')

        token_id = id or str(uuid.uuid4())

        BaseToken.__init__(self,
                          token_id=token_id,
                          token_subject='application_token',
                          token_audience=application,
                          token_expiration=None,
                          token_created=self.created)

class DeviceToken(BaseToken):

    def __init__(self,
                 id=None,
                 device=None,
                 status=None,
                 created=None):

        self.status = status
        self.created = created or datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')

        token_id = id or str(uuid.uuid4())

        BaseToken.__init__(self,
                          token_id=token_id,
                          token_subject='device_token',
                          token_audience=device,
                          token_expiration=None,
                          token_created=self.created)

class RefreshToken(BaseToken):

    def __init__(self,
                 id=None,
                 user_id=None,
                 expiration=None,
                 created=None):

        self.created = created or datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')

        token_id = id or str(uuid.uuid4())
        token_expiration = expiration or (
            datetime.datetime.utcnow() + datetime.timedelta(minutes=15)
        ).strftime('%Y-%m-%d %H:%M:%S')

        BaseToken.__init__(self,
                          token_id=token_id,
                          token_subject='refresh_token',
                          token_audience=user_id,
                          token_expiration=token_expiration,
                          token_created=self.created)
