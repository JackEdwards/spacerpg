#!/usr/bin/env python3.6
import flask
from functools import wraps
import inspect
import json

#  Import models and modules
print('Startup: Initializing')
import os
import sys
from . import services
app_path = sys.path[0]
project_path = os.path.dirname(app_path)
sys.path.append(project_path)
from ObjectModels import UserObject
from ObjectModels import TokenObject
from DataModels import UserRepo
from DataModels import TokenRepo
from Modules import Logger
from Modules import Token

#  Load app config
print('Startup: Loading config.json')
config_file = os.path.join(app_path, 'config.json')
with open(config_file) as json_data:
    config = json.load(json_data)

#  Initialize and config the logger
print('Startup: Configuring the logger')
logger = Logger()
logger.config['LOG_FILE_DIRECTORY'] = config['log_config']['log_path'] or app_path
logger.config['WRITE_DEBUG_LOGS'] = config['log_config']['debug']
logger.config['WRITE_TRACE_LOGS'] = config['log_config']['trace']
print('Startup: Handing off logging duties to the logger...')
logger.log(level=3, message='App start')

app = flask.Flask(__name__)

#  Set app config
app.debug = config['app_config']['debug']
# app.secret_key = config['app_config']['rotating_secret_key']

def add_cors_headers(response):
    response.headers['Access-Control-Allow-Origin'] = config['app_config']['allowed_origins']
    return response

app.after_request(add_cors_headers)

#  Decorators
#  http://flask.pocoo.org/docs/0.12/patterns/viewdecorators/
def require_valid_user_token(func):
    @wraps(func)
    def inner(*args, **kwargs):
        if not 'Authorization-User' in flask.request.headers:
            return flask.jsonify(
                success=False,
                error='missing_user_token'
            )
        else:
            token = flask.request.headers['Authorization-User']
            decoded_token = Token().decode(
                token=token,
                key=config['app_config']['rotating_secret_key'],
                algorithm='HS256'
            )
            if decoded_token['verified']:
                pass
                # todo, verify the token is not expired
            else:
                logger.log(level=0, message='Invalid token', exception=decoded_token)
                return flask.jsonify(
                    success=False,
                    error='invalid_user_token'
                )
        return func(*args, **kwargs)
    return inner

def require_valid_app_token(func):
    @wraps(func)
    def inner(*args, **kwargs):
        if not 'Authorization-App' in flask.request.headers:
            return flask.jsonify(
                success=False,
                error='missing_application_token'
            )
        else:
            # todo, actually validate the token
            pass
        return func(*args, **kwargs)
    return inner

#  API views
@app.route('/', methods=['GET'])
def index():
    logger.log(level=3, message='Loading view')
    return "Hello world"

@app.route('/get-app-token', methods=['POST'])
def get_app_token():
    logger.log(level=3, message='Loading view')
    data = flask.request.get_json()
    if not data['key']:
        logger.log(level=4, message='Key not in data')
        return flask.jsonify(
            success=False,
            error='missing_key'
        )

@app.route('/get-user-token', methods=['POST'])
@require_valid_app_token
def get_user_token():
    logger.log(level=3, message='Loading view')
    data = flask.request.get_json()
    problems = []

    #  Validate the data
    if not data['username']:
        logger.log(level=4, message='Username not in data')
        problems.append({
            'field': 'username',
            'message': 'missing_username'
        })

    if not data['password']:
        logger.log(level=4, message='Password not in data')
        problems.append({
            'field': 'password',
            'message': 'missing_password'
        })

    if len(problems) > 0:
        return flask.jsonify(
            success=False,
            errors=problems
        )
    else:  # Data is valid; see if the credentials exist
        user = UserRepo().get_by_username(data['username'])
        if user:
            logger.log(level=4, message='Username exists: [{}]'.format(data['username']))
            if user.passwords_match(config['app_config']['static_secret_key'], data['password']):
                logger.log(level=4, message='Passwords match')
                #  Credentials are valid; give the user a token
                token = TokenObject(user_id=user.id)
                TokenRepo().insert(token)
                encoded_token = Token().encode(
                    key=config['app_config']['rotating_secret_key'],
                    payload=token.payload,
                    algorithm='HS256'
                )

                return flask.jsonify(
                    success=True,
                    messages=['Login success'],
                    token_data={
                        'token': encoded_token,
                        'token_type': 'Bearer'
                    }
                )
            else:
                logger.log(level=4, message='Password does not match')
                problems.append({
                    'field': 'generic',
                    'message': 'invalid_credentials'
                })
                return flask.jsonify(
                    success=False,
                    errors=problems
                )
        else:
            logger.log(level=4, message='Username does not exist: [{}]'.format(data['username']))
            problems.append({
                'field': 'generic',
                'message': 'invalid_credentials'
            })
            return flask.jsonify(
                success=False,
                errors=problems
            )

@app.route('/refresh-user-token', methods=['POST'])
@require_valid_app_token
@require_valid_user_token
def refresh_user_token():
    logger.log(level=3, message='Loading view')
    token = flask.request.headers['Authorization-User']
    decoded_token = Token().decode(
        token=token,
        key=config['app_config']['rotating_secret_key'],
        algorithm='HS256'
    )
    #  Get the token id
    payload = decoded_token['Payload']
    token_id = payload['jti']
    
    old_token_object = TokenRepo().get_by_id(token_id)
    if old_token_object:
        logger.log(level=4, message='Original token found; issuing new token')
        new_token_object = TokenObject(user_id=old_token_object.user_id)
        TokenRepo().insert(new_token_object)
        encoded_token = Token().encode(
            key=config['app_config']['rotating_secret_key'],
            payload=new_token_object.payload,
            algorithm='HS256'
        )
        #  Remove the old token from the db
        TokenRepo().delete_by_id(token_id)

        #  Send the new token to the user
        return flask.jsonify(
            success=True,
            messages=['Login success'],
            token_data={
                'token': encoded_token,
                'token_type': 'Bearer'
            }
        )

@app.route('/create-user', methods=['POST'])
@require_valid_app_token
def create_user():
    logger.log(level=3, message='Loading view')
    data = flask.request.get_json()
    problems = []

    #  Validate the data
    if not data['username']:
        logger.log(level=4, message='Username not in data')
        problems.append({
            'field': 'username',
            'message': 'missing_username'
        })
    else:
        if UserRepo().get_by_username(data['username']):
            logger.log(level=4, message='Username already exists: [{}]'.format(data['username']))
            problems.append({
                'field': 'username',
                'message': 'username_already_exists'
            })

    if not data['email']:
        logger.log(level=4, message='Email not in data')
        problems.append({
            'field': 'email',
            'message': 'missing_email'
        })
    else:
        if UserRepo().get_by_email(data['email']):
            logger.log(level=4, message='Email already exists: [{}]'.format(data['email']))
            problems.append({
                'field': 'email',
                'message': 'email_already_exists'
            })

    if not data['password']:
        logger.log(level=4, message='Password not in data')
        problems.append({
            'field': 'password',
            'message': 'missing_password'
        })

    if len(problems) > 0:
        return flask.jsonify(
            success=False,
            errors=problems
        )
    else:  # No problems; try to create the user
        try:
            logger.log(level=3, message='Creating user [{}] with email [{}]'.format(data['username'], data['email']))
            user = UserObject(
                username=data['username'],
                display_name=data['username'],
                password=data['password'],
                email=data['email']
            )
            user.hash_password(config['app_config']['static_secret_key'])
            result = UserRepo().insert(user)
            if result:
                logger.log(level=3, message='User added to the database')
                return flask.jsonify(
                    success=True,
                    messages=["user_created"]
                )
            else:
                logger.log(level=3, message='Failed to add user to the database')
                problems.append({
                    'field': 'server_fault',
                    'message': 'server_error'
                })
                return flask.jsonify(
                    success=False,
                    errors=problems
                )
        except Exception as e:
            logger.log(level=2, message='Exception while creating user', exception=e)
            problems.append({
                'field': 'server_fault',
                'message': 'server_error'
            })
            return flask.jsonify(
                success=False,
                errors=problems
            )
    
#  Run the app
if __name__ == '__main__':
    app.run(host=config['app_config']['base_url'], port=config['app_config']['port'])
