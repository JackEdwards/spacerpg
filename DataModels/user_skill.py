import sys
from . import Database
sys.path.append('..')
from ObjectModels import UserSkillObject


class UserSkillRepo:

    def __init__(self):
        self.db = Database()
        self.create_table_if_not_exists()

    #  Convert a single record to a UserSkillObject
    def deserialize(self, named_record):
        keys = named_record.keys()
        userskill_object = UserSkillObject()
        for key in keys:
            setattr(userskill_object, key, named_record[key])
        return userskill_object

    #  Convert a UserSkillObject into a dict
    def serialize(self, userskill_object):
        userskill_dict = {}
        for key, value in userskill_object.__dict__.items():
            userskill_dict[key] = value
        return userskill_dict

    #  Create a "user_skill" record for the passed in UserSkillObject
    def insert(self, userskill_object):
        query = ('INSERT INTO user_skill ('
            'id,'
            'user_id,'
            'skill_id'
            ') VALUES ('
            ':id,'
            ':user_id,'
            ':skill_id,'
            ');')
        args = self.serialize(userskill_object)
        self.db.update(query, args)

    #  Get all "user_skill" records
    def get_all(self):
        query = ('SELECT * FROM user_skill')
        named_records = self.db.query(query)

        #  De-serialize the result
        userskill_objects = []
        for record in named_records:
            userskill_objects.append(self.deserialize(record))
        return userskill_objects

    #  Get a "user_skill" record by it's "id"
    def get_by_id(self, key):
        query = ('SELECT * FROM user_skill WHERE id=:id;')
        args = {'id': key}
        named_record = self.db.query(query, args)

        #  De-serialize the result
        userskill_object = self.deserialize(named_record)
        return userskill_object

    #  Get all "user_skill" records for the passed in "user_id"
    def get_by_user_id(self, user_id):
        query = ('SELECT * FROM user_skill WHERE user_id=:user_id;')
        args = {'user_id': user_id}
        named_records = self.db.query(query, args)

        #  De-serialize the result
        userskill_objects = []
        for record in named_records:
            userskill_objects.append(self.deserialize(record))
        return userskill_objects

    #  Get a "user_skill" record by it's "skill_id"
    def get_by_skill_id(self, skill_id):
        query = ('SELECT * FROM user_skill WHERE skill_id=:skill_id')
        args = {'skill_id': skill_id}
        named_record = self.db.query(query, args)

        #  De-serialize the result
        userskill_object = self.deserialize(named_record)
        return userskill_object

    #  Delete a "user_skill" record by it's "id"
    def delete_by_id(self, key):
        query = ('DELETE FROM user_skill WHERE id=:id;')
        args = {'id': key}
        result = self.db.update(query, args)

    #  Delete all "user_skill" records for the passed in "user_id"
    def delete_by_user_id(self, user_id):
        query = ('DELETE FROM user_skill WHERE user_id=:user_id;')
        args = {'user_id': user_id}
        result = self.db.update(query, args)

    #  Delete a "user_skill" record by it's "skill_id"
    def delete_by_skill_id(self, skill_id):
        query = ('DELETE FROM user_skill WHERE skill_id=:skill_id;')
        args = {'skill_id': skill_id}
        result = self.db.update(query, args)

    #  Create the "user_skill" table if it does not exist
    def create_table_if_not_exists(self):
        query = ('CREATE TABLE IF NOT EXISTS user_skill ('
            'id TEXT PRIMARY KEY,'
            'user_id,'
            'skill_id UNIQUE);')
        self.db.update(query)
        