import sys
from . import Database
sys.path.append('..')
from ObjectModels import UserObject


class UserRepo:

    def __init__(self):
        self.db = Database()
        self.create_table_if_not_exists()

    #  Convert a single record to a UserObject
    def deserialize(self, named_record):
        keys = named_record[0].keys()
        user_object = UserObject()
        for key in keys:
            setattr(user_object, key, named_record[0][key])
        return user_object

    #  Convert a UserObject into a dict
    def serialize(self, user_object):
        user_dict = {}
        for key, value in user_object.__dict__.items():
            user_dict[key] = value
        return user_dict

    #  Create a "user" record for the passed in UserObject
    def insert(self, user_object):
        query = ('INSERT INTO user ('
                'id,'
                'username,'
                'display_name,'
                'password,'
                'email,'
                'email_confirm,'
                'energy,'
                'credits,'
                'cash,'
                'active,'
                'last_active,'
                'last_update,'
                'created'
                ') VALUES ('
                ':id,'
                ':username,'
                ':display_name,'
                ':password,'
                ':email,'
                ':email_confirm,'
                ':energy,'
                ':credits,'
                ':cash,'
                ':active,'
                ':last_active,'
                ':last_update,'
                ':created'
                ');')
        args = self.serialize(user_object)
        result = self.db.update(query, args)
        return result

    #  Get all "user" records
    def get_all(self):
        query = ('SELECT * FROM user')
        named_records = self.db.query(query)

        #  De-serialize the result
        user_objects = []
        for record in named_records:
            user_objects.append(self.deserialize(record))
        return user_objects

    #  Get a "user" record by it's "id"
    #  The "id" column has the "PRIMARY KEY" constraint
    def get_by_id(self, key):
        query = ('SELECT * FROM user WHERE id=:id;')
        args = {'id': key}
        named_records = self.db.query(query, args)
        if len(named_records) > 0:
            #  De-serialize the result
            user_object = self.deserialize(named_records)
            return user_object
        else:
            return None

    #  Get a "user" record by it's "username"
    #  The "username" column has the "UNIQUE" constraint
    def get_by_username(self, username):
        #  Query the DB for the user
        query = ('SELECT * FROM user WHERE username=:username;')
        args = {'username': username}
        named_records = self.db.query(query, args)
        if len(named_records) > 0:
            #  De-serialize the result
            user_object = self.deserialize(named_records)
            return user_object
        else:
            return None

    #  Get a "user" record by it's "email"
    #  The "email" column has the "UNIQUE" constraint
    def get_by_email(self, email):
        #  Query the DB for the user
        query = ('SELECT * FROM user WHERE email=:email;')
        args = {'email': email}
        named_records = self.db.query(query, args)
        if len(named_records) > 0:
            #  De-serialize the result
            user_object = self.deserialize(named_records)
            return user_object
        else:
            return None

    #  Update the "user" record for the passed in UserObject
    #  Right now all columns can be updated
    #  Let the application decide whether to update "protected" columns
    def update(self, user_object):
        query = ('UPDATE user SET '
            '\'username\'=:username,'
            '\'display_name\'=:display_name,'
            '\'password\'=:password,'
            '\'email\'=:email,'
            '\'email_confirm\'=:email_confirm,'
            '\'energy\'=:energy,'
            '\'credits\'=:credits,'
            '\'cash\'=:cash,'
            '\'active\'=:active,'
            '\'last_active\'=:last_active,'
            '\'last_update\'=:last_update,'
            'WHERE id=:id;')
        args = self.serialize(user_object)
        result = self.db.update(query, args)

    #  Delete a "user" record by it's "id"
    def delete_by_id(self, key):
        query = ('DELETE FROM user WHERE id=:id;')
        args = {'id': key}
        result = self.db.update(query, args)

    #  Create the "user" table if it does not exist
    def create_table_if_not_exists(self):
        query = ('CREATE TABLE IF NOT EXISTS user ('
            'id TEXT PRIMARY KEY,'
            'username UNIQUE,'
            'display_name UNIQUE,'
            'password,'
            'email UNIQUE,'
            'email_confirm,'
            'energy,'
            'credits,'
            'cash,'
            'active,'
            'last_active,'
            'last_update,'
            'created);')
        self.db.update(query)
