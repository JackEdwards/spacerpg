import sys
from . import Database
sys.path.append('..')
from ObjectModels import SkillObject


class SkillRepo:

    def __init__(self):
        self.db = Database()
        self.create_table_if_not_exists()

    #  Convert a single record to a SkillObject
    def deserialize(self, named_record):
        keys = named_record.keys()
        skill_object = SkillObject()
        for key in keys:
            setattr(skill_object, key, named_record[key])
        return skill_object

    #  Convert a SkillObject into a dict
    def serialize(self, skill_object):
        skill_dict = {}
        for key, value in skill_object.__dict__.items():
            skill_dict[key] = value
        return skill_dict

    #  Create a "skill" record for the passed in SkillObject
    def insert(self, skill_object):
        query = ('INSERT INTO skill ('
                'id,'
                'enum,'
                'level,'
                'experience,'
                'difficulty,'
                'last_update',
                'created'
                ') VALUES ('
                ':id,'
                ':enum,'
                ':level,'
                ':experience,'
                ':difficulty,'
                ':last_update,'
                ':created'
                ');')
        args = self.serialize(skill_object)
        self.db.update(query, args)

    #  Create the "skill" table if it does not exist
    def create_table_if_not_exists(self):
        query = ('CREATE TABLE IF NOT EXISTS skill ('
            'id TEXT PRIMARY KEY,'
            'enum',
            'level,'
            'experience,'
            'difficulty,'
            'last_update,'
            'created);')
        self.db.update(query)