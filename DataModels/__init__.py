import sqlite3
import json
import os
import sys
app_path = sys.path[0]
project_path = os.path.dirname(app_path)
sys.path.append(project_path)
from Modules import Logger


class Database:

    def __init__(self):
        config_file = os.path.join(app_path, 'config.json')
        with open(config_file) as json_data:
            config = json.load(json_data)
        
        self.logger = Logger()
        self.logger.config['LOG_FILE_DIRECTORY'] = config['log_config']['log_path'] or app_path
        self.logger.config['WRITE_DEBUG_LOGS'] = config['log_config']['debug']
        self.logger.config['WRITE_TRACE_LOGS'] = config['log_config']['trace']
        self.connection = sqlite3.connect(config['database_config']['sqlite3_config']['file_path'] or os.path.join(app_path, 'main.db'))
        self.connection.row_factory = sqlite3.Row # This let's us do cursor.fetchone().keys()
        self.cursor = self.connection.cursor()

    #  "SELECT"
    def query(self, query, args={}):
        self.logger.log(level=5, message='query: [{}] | args: [{}]'.format(query, args))
        try:
            self.cursor.execute(query, args)
            return self.cursor.fetchall()
        except sqlite3.Error as e:
            self.logger.log(level=2, message='Exception occurred querying the database', exception=e)

    #  "CREATE", "INSERT", "UPDATE"
    def update(self, query, args={}):
        self.logger.log(level=5, message='query: [{}] | args: [{}]'.format(query, args))
        try:
            result = self.cursor.execute(query, args)
            self.connection.commit()
            return True
        except sqlite3.Error as e:
            self.logger.log(level=2, message='Exception occurred updating the database', exception=e)
            self.connection.rollback()
            return False

    def __del__(self):
        self.connection.close()


from DataModels.user import UserRepo
from DataModels.skill import SkillRepo
from DataModels.user_skill import UserSkillRepo
from DataModels.token import RefreshTokenRepo
