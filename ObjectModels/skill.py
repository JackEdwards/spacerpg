import datetime
import uuid

class SkillObject:

    # Default values are only evaluated once, when the definition is executed
    # Thus, the (self.x = x or 'value') pattern is necessary for dynamic values
    def __init__(self,
                 key=None,
                 enum=0,
                 level=0,
                 experience=0,
                 difficulty=1,
                 last_update=None,
                 created=None):

        #  Skill Properties
        self.id = key or str(uuid.uuid4())
        self.enum = enum
        self.level = level
        self.experience = experience
        self.difficulty = difficulty
        self.last_update = last_update or datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')
        self.created = created or datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')
