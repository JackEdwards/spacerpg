from math import sqrt
import uuid
from .. import ReactorModels
from .. import ThrusterModels


class Ship:

    item_type = 'Ship'

    def __init__(self, owner_id, name, ship_class, ship_size, base_mass, compartments, component_slots):
        self.id = uuid.uuid4()
        self.owner_id = owner_id
        self.name = name
        self.ship_class = ship_class
        self.ship_size = ship_size
        self.base_mass = base_mass
        self.compartments = compartments
        self.component_slots = component_slots
        self.sector = None  #  This must be an object

    def add_compartment(self):
        self.compartments.append(Compartment(self.id))

    def add_reactor_slot(self):
        self.component_slots.append(ReactorSlot(self.id))

    def add_thruster_slot(self):
        self.component_slots.append(ThrusterSlot(self.id))

    def system_check(self, role):
        for compartment in self.compartments:
            if compartment.system_check(role):
                return True

    def component_slot_check(self, slot_type):
        for component_slot in self.component_slots:
            if component_slot.slot_type is slot_type:
                return True

    def travel_to_sector(self, sector):
        if self.sector is None:
            raise Exception('Cannot travel to sector; Ship is not in a sector')
        else:
            x = abs(self.sector.x - sector.x)
            y = abs(self.sector.y - sector.y)
            distance = sqrt(x*x + y*y)

    @property
    def total_mass(self):
        x = self.base_mass
        for component_slot in self.component_slots:
            if component_slot.installed_component:
                x += component_slot.installed_component.mass
        return x

    @property
    def max_thrust(self):
        x = 0
        for component_slot in self.component_slots:
            if component_slot.installed_component:
                if component_slot.installed_component.component_type is 'Thruster':
                    x += component_slot.installed_component.thrust
        return x

    @property
    def max_velocity(self):
        x = self.max_thrust / self.total_mass
        return x


#  Ship compartments are rooms inside the ship
#  Ship systems are installed in compartments
class Compartment:
    def __init__(self, ship_id):
        self.id = uuid.uuid4()
        self.ship_id = ship_id
        self.powered = 0
        self.oxygenated = 0
        self.crew_capacity = 0
        self.max_health = 0
        self.health = 0
        self.systems = []

    def add_system(self, role):
        self.systems.append(System(self.id, role))

    def system_check(self, role):
        for system in self.systems:
            if system.role is role:
                return True


#  Ship systems control the hardware installed in or on the ship
#  If the system is destroyed, the hardware does not work
class System:
    def __init__(self, compartment_id, role):
        self.id = uuid.uuid4()
        self.compartment_id = compartment_id
        self.role = role
        self.max_health = 0
        self.health = 0


#  Ship components are pieces of hardware installed in or on the ship
#  Each component gets installed in a slot
class ComponentSlot:
    def __init__(self, ship_id, slot_type):
        self.id = uuid.uuid4()
        self.ship_id = ship_id
        self.slot_type = slot_type
        self.installed_component = None

    def install_component(self, component):
        if component.component_type is self.slot_type:
            if self.installed_component is None:
                self.installed_component = component

                #  Remove the component from inventory
                #  ... unless the ship is being instantiated with basic components
                pass

            else:
                raise Exception('Cannot install component; slot is already occupied')
        else:
            raise Exception('Cannot install component; type mismatch')

    def uninstall_component(self):
        self.installed_component = None

        #  Return the component to inventory
        pass


class ThrusterSlot(ComponentSlot):
    def __init__(self, ship_id):
        ComponentSlot.__init__(self, ship_id, 'Thruster')


class WeaponSlot(ComponentSlot):
    def __init__(self, ship_id):
        ComponentSlot.__init__(self, ship_id, 'Weapon')


class ReactorSlot(ComponentSlot):
    def __init__(self, ship_id):
        ComponentSlot.__init__(self, ship_id, 'Reactor')


#  Import the individual ship models
from .shuttle import Shuttle
from .scout import Scout
