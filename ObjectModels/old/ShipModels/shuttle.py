from . import Ship
from . import ReactorModels
from . import ThrusterModels


#  A type of Ship
class Shuttle(Ship):
    def __init__(self, owner_id, name):
        Ship.__init__(self, owner_id, name, 'Civilian', 'Tiny', 200, [], [])

        #  Ability:  "Nimble" - High chance to dodge

        #  Add an internal compartment
        self.add_compartment()

        #  Fill the compartment with systems
        self.compartments[0].add_system('Reactor')
        self.compartments[0].add_system('Propulsion')
        self.compartments[0].add_system('Bridge')
        self.compartments[0].add_system('Oxygen')
        self.compartments[0].add_system('Communication')
        self.compartments[0].add_system('Navigation')

        #  Add component slots
        self.add_reactor_slot()
        self.add_thruster_slot()

        #  Fill the component slots with default components
        for component_slot in self.component_slots:
            if component_slot.slot_type is 'Reactor':
                component_slot.install_component(ReactorModels.BasicTiny(self.owner_id))

            if component_slot.slot_type is 'Thruster':
                component_slot.install_component(ThrusterModels.BasicTiny(self.owner_id))
