from . import Ship


#  A type of Ship
class Scout(Ship):
    def __init__(self, owner_id, name):
        Ship.__init__(self, owner_id, name, 'Expedition', 'Tiny', 350, [], [])
        self.add_compartment()
        self.compartments[0].add_system('Reactor')
        self.compartments[0].add_system('Engine')
        self.compartments[0].add_system('Bridge')
        self.compartments[0].add_system('Ventilation')
        self.compartments[0].add_system('Radio')
        self.compartments[0].add_system('Navigation')
        self.compartments[0].add_system('Weapon')

        #  Ability:  Enhanced radar
