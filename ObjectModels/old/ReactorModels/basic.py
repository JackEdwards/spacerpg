from . import Basic


class Tiny(Basic):
    def __init__(self, owner_id):
        Basic.__init__(self, owner_id, 'Tiny', 50)
        self.mass = 5
