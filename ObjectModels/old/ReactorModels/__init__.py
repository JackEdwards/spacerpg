import uuid


class Reactor:

    item_type = 'Component'
    component_type = 'Reactor'

    def __init__(self, owner_id, name, size, power_output):
        self.id = uuid.uuid4()
        self.owner_id = owner_id
        self.name = name
        self.size = size
        self.power_output = power_output


class Basic(Reactor):
    def __init__(self, owner_id, size, power_output):
        Reactor.__init__(self, owner_id, 'Basic Reactor', size, power_output)


#  Import the "Basic" reactors
from .basic import Tiny as BasicTiny
