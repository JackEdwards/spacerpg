import uuid


class Sector:

    def __init__(self, x, y):
        self.id = uuid.uuid4()
        self.x = x
        self.y = y
