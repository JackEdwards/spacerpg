from . import Basic


class Tiny(Basic):
    def __init__(self, owner_id):
        Basic.__init__(self, owner_id, 'Tiny', 2, 10)
        self.mass = 20


class Small(Basic):
    def __init__(self, owner_id):
        Basic.__init__(self, owner_id, 'Small', 5, 25)
        self.mass = 50


class Medium(Basic):
    def __init__(self, owner_id):
        Basic.__init__(self, owner_id, 'Medium', 12, 60)
        self.mass = 120


class Large(Basic):
    def __init__(self, owner_id):
        Basic.__init__(self, owner_id, 'Large', 26, 130)
        self.mass = 260
