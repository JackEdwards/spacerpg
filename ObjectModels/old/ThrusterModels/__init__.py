import uuid


class Thruster:

    item_type = 'Component'
    component_type = 'Thruster'

    def __init__(self, owner_id, name, size, thrust, power_draw):
        self.id = uuid.uuid4()
        self.owner_id = owner_id
        self.name = name
        self.size = size
        self.thrust = thrust  # in kiloNewtons
        self.power_draw = power_draw


class Basic(Thruster):
    def __init__(self, owner_id, size, thrust, power_draw):
        Thruster.__init__(self, owner_id, 'Basic Thruster', size, thrust, power_draw)


#  Import the "Basic" engines
from .basic import Tiny as BasicTiny
from .basic import Small as BasicSmall
from .basic import Medium as BasicMedium
from .basic import Large as BasicLarge
