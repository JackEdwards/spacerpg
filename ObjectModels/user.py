import datetime
from hashlib import sha256
import hmac
import uuid

class UserObject:

    # Default values are only evaluated once, when the definition is executed
    # Thus, the (self.x = x or 'value') pattern is necessary for dynamic values
    def __init__(self,
                 key=None,
                 username=None,
                 display_name=None,
                 password=None,
                 email=None,
                 email_confirm=False,
                 energy=0,  # "action points" or "ticks"
                 credits=0, # Regular currency
                 cash=0,    # Premium currency
                 active=False,
                 last_active=None,
                 last_update=None,
                 created=None):

        #  User properties
        self.id = key or str(uuid.uuid4())
        self.username = username
        self.display_name = display_name
        self.password = password
        self.email = email
        self.email_confirm = email_confirm
        self.energy = energy
        self.credits = credits
        self.cash = cash
        self.active = active
        self.last_active = last_active
        self.last_update = last_update or datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')
        self.created = created or datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')

    #  Hash the user's password when the user is first created
    #  Never do this more than once for the same user
    def hash_password(self, salt):
        key = str(self.id + salt).encode('utf-8')
        message = str(self.password).encode('utf-8')
        self.password = hmac.new(key, message, sha256).hexdigest()

    #  Compare the passed in 'password' to the user's hashed password
    def passwords_match(self, salt, password):
        key = str(self.id + salt).encode('utf-8')
        message = str(password).encode('utf-8')
        hashed_password = hmac.new(key, message, sha256).hexdigest()
        if hmac.compare_digest(hashed_password, self.password):
            return True
        else:
            return False