import datetime
import uuid

class UserSkillObject:

    # Default values are only evaluated once, when the definition is executed
    # Thus, the (self.x = x or 'value') pattern is necessary for dynamic values
    def __init__(self,
                 key=None,
                 user_id=None,
                 skill_id=None):

        #UserSkill properties
        self.id = key or str(uuid.uuid4())
        self.user_id = user_id
        self.skill_id = skill_id
