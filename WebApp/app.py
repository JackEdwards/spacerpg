import flask
from functools import wraps
import inspect
import json

#  Import models and modules
print('Startup: Initializing')
import os
import sys
app_path = sys.path[0]
project_path = os.path.dirname(app_path)
sys.path.append(project_path)
from ObjectModels import UserObject
from ObjectModels import TokenObject
from DataModels import UserRepo
from DataModels import TokenRepo
from Modules import Logger
from Modules import Token

#  Load app config
print('Startup: Loading config.json')
config_file = os.path.join(app_path, 'config.json')
with open(config_file) as json_data:
    config = json.load(json_data)

#  Initialize and config the logger
print('Startup: Configuring the logger')
logger = Logger()
logger.config['LOG_FILE_DIRECTORY'] = config['log_config']['log_path'] or app_path
logger.config['WRITE_DEBUG_LOGS'] = config['log_config']['debug']
logger.config['WRITE_TRACE_LOGS'] = config['log_config']['trace']
print('Startup: Handing off logging duties to the logger...')
logger.log(level=3, message='App start')

app = flask.Flask(__name__)

#  Set app config
app.debug = config['app_config']['debug']
# app.secret_key = config['app_config']['rotating_secret_key']

#  Decorators
#  http://flask.pocoo.org/docs/0.12/patterns/viewdecorators/
def require_valid_session(func): # Front-end decorator
    @wraps(func)
    def inner(*args, **kwargs):
        if not 'token' in flask.session:
            return flask.redirect(flask.url_for('login'))
        else:
            # todo, actually validate the token
            pass
        return func(*args, **kwargs)
    return inner

#  Front-end views
@app.route('/')
@require_valid_session
def index():
    return "Hello world"

@app.route('/login')
def login():
    logger.log(level=3, message='Loading view')
    return flask.render_template('login.html', title=config['app_config']['site_name'])
    
#  Run the app
if __name__ == '__main__':
    app.run(debug=True, host=config['app_config']['base_url'], port=config['app_config']['port'])
