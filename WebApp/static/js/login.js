document.addEventListener("DOMContentLoaded", function() {
    console.log("login.js:  Document ready");

    var divLogin = document.getElementById("login-div");
    var divCreate = document.getElementById("create-div");
    var ulLoginResponse = document.getElementById("login-response");
    var ulCreateResponse = document.getElementById("create-response");

    //  Toggle view from "Login" to "Create User"
    var btnCreateUserSwitch = document.getElementById("switch-to-create-user");
    btnCreateUserSwitch.addEventListener("click", function() {
        divLogin.style.display = "none";
        divCreate.style.display = "block";
        ulLoginResponse.classList.remove("slide-down");
    }, false);

    //  Toggle view from "Create User" back to "Login"
    var btnLoginSwitch = document.getElementById("switch-to-login");
    btnLoginSwitch.addEventListener("click", function() {
        divLogin.style.display = "block";
        divCreate.style.display = "none";
        ulCreateResponse.classList.remove("slide-down");
    }, false);

    //  Process the "Login" request
    var btnLoginSubmit = document.getElementById("login-submit");
    btnLoginSubmit.addEventListener("click", function(event) {
        //  Suppress the "submit" default behavior
        event.preventDefault();

        //  UX
        ulLoginResponse.classList.remove("slide-down");
        ulLoginResponse.innerHTML = "";
        
        //  Gather data
        var username = document.getElementById("login-username-input").value;
        var password = document.getElementById("login-password-input").value;
        var data = {
            "username": username,
            "password": password
        };

        //  Send the data; callback handles the response
        postJsonData(data, "http://127.0.0.1:5000/api/login", loginCallback);
    }, false);

    //  Process the "Create User" request
    var btnCreateSubmit = document.getElementById("request-access-submit");
    btnCreateSubmit.addEventListener("click", function(event) {
        //  Suppress the "submit" default behavior
        event.preventDefault();

        //  UX
        ulCreateResponse.classList.remove("slide-down");
        ulCreateResponse.innerHTML = "";

        //  Gather data
        var username = document.getElementById("create-username-input").value;
        var email = document.getElementById("create-email-input").value;
        var password = document.getElementById("create-password-input").value;
        var confirm = document.getElementById("create-confirm-input").value;
        var data = {
            "username": username,
            "email": email,
            "password": password,
            "confirm": confirm
        };

        //  Send the data; callback handles the response
        postJsonData(data, "http://127.0.0.1:5000/api/create-user", createCallback);
    }, false);

    //  Handle the "Login" response from the server
    //  If the request was successful, save the token in cookies then navigate away from the login page
    function loginCallback(data) {
        if (data.success) {
            var token = data.token_data.token;
            var payload = JSON.parse(atob(token.split(".")[1]));
            document.cookie = "token=".concat(token);
            localStorage.setItem("token", token);
            localStorage.setItem("tokenExpiration", payload.exp);
            navigateWithToken(localStorage.getItem("token"), "http://127.0.0.1:5000/");
        } else {
            for (var i = 0; i < data.errors.length; i++) {
                var li = document.createElement("li");
                li.appendChild(document.createTextNode(data.errors[i].message));
                ulLoginResponse.appendChild(li);
            }
            ulLoginResponse.classList.add("slide-down");
        }
    }

    //  Handle the "Create User" response from the server
    function createCallback(data) {
        if (data.success) {
            for (var i = 0; i < data.messages.length; i++) {
                var li = document.createElement("li");
                li.appendChild(document.createTextNode(data.messages[i]));
                ulCreateResponse.appendChild(li);
            }
        } else {
            for (var i = 0; i < data.errors.length; i++) {
                var li = document.createElement("li");
                li.appendChild(document.createTextNode(data.errors[i].message));
                ulCreateResponse.appendChild(li);
            }
        }
        ulCreateResponse.classList.add("slide-down");
    }
});

//  Generic function for POST requests
function postJsonData(data, url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open("POST", url, true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onreadystatechange = function() {
        if (xhr.readyState === 4 && xhr.status === 200) {
            callback(JSON.parse(xhr.responseText));
        }
    }
    xhr.send(JSON.stringify(data));
}